//
//  SelectGroupViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/6/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Segmentio

class SelectGroupViewController: UIViewController {
    
    var question: QuestionModel! {
        didSet {
            refreshUI()
        }
    }
    
    @IBOutlet weak var questionName: UILabel!
    @IBOutlet weak var groupLeftButton: UIButton!
    @IBOutlet weak var groupRightButton: UIButton!
    @IBOutlet weak var battlegroundButton: UIButton!
    
    func refreshUI() {
        questionName.text = question.questionName
//        groupLeftButton.setAttributedTitle(NSAttributedString(string: question.groupLeft.groupName, attributes: Constants.GroupNameAttributes), for: .normal)
//        groupLeftButton.setAttributedTitle(NSAttributedString(string: question.groupRight.groupName, attributes: Constants.GroupNameAttributes), for: .normal)
//        battlegroundButton.setAttributedTitle(NSAttributedString(string: question.battleground.groupName, attributes: Constants.GroupNameAttributes), for: .normal)
        groupLeftButton.setTitle(question.groupLeft.groupName + "\n" + question.groupLeft.groupDescription, for: .normal)
        groupRightButton.setTitle(question.groupRight.groupName + "\n" + question.groupRight.groupDescription, for: .normal)
        battlegroundButton.setTitle(question.battleground.groupName + "\n" + question.battleground.groupDescription, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtons()
        
        if question != nil {
            refreshUI()
        }
        
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupButtons() {
        groupLeftButton.layer.cornerRadius = 10
        groupRightButton.layer.cornerRadius = 10
        battlegroundButton.layer.cornerRadius = 10
        groupLeftButton.titleLabel?.lineBreakMode = .byWordWrapping
        groupLeftButton.titleLabel?.textAlignment = .center
        groupRightButton.titleLabel?.lineBreakMode = .byWordWrapping
        groupRightButton.titleLabel?.textAlignment = .center
        battlegroundButton.titleLabel?.lineBreakMode = .byWordWrapping
        battlegroundButton.titleLabel?.textAlignment = .center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectGroupAction(_ sender: UIButton) {
        performSegue(withIdentifier: "EnterChatSegue", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ChatViewController
        if let buttonSender = sender as? UIButton {

            if buttonSender === groupLeftButton {
                destination.chatGroup = question.groupLeft
            }
            else if buttonSender === groupRightButton {
                destination.chatGroup = question.groupRight
            }
            else {
                destination.chatGroup = question.battleground
            }

            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
