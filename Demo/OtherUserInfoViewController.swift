//
//  OtherUserInfoViewController.swift
//  Demo
//
//  Created by Michael Zhang on 12/6/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import ChameleonFramework

class OtherUserInfoViewController: FormViewController {
    
    var userProfile: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.navigationItem.title = "New title"
        //        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back",
        //                                                                style: .plain,
        //                                                                target: nil,
        //                                                                action: nil
        
        navigationOptions = RowNavigationOptions.Enabled.union(.StopDisabledRow)
        form +++ Section()
            <<< ViewRow<UIView>() { row in
            }
                .cellSetup { (cell, row) in
                    cell.height = { return CGFloat(150) }
                    cell.contentView.backgroundColor = UIColor.clear
                    let profileImageView = UIImageView(frame: CGRect(x: cell.contentView.frame.width/2 - 55/2, y: cell.contentView.frame.height/2, width: 110, height: 110))
                    profileImageView.image = self.userProfile?.profileImage
                    profileImageView.layer.cornerRadius = profileImageView.frame.height/2
                    profileImageView.clipsToBounds = true
                    cell.contentView.addSubview(profileImageView)
                    
        }
        
        form +++ Section("Basic Info")
            <<< NameRow(Constants.Keys.FirstNameKey){ row in
                row.title = "First Name"
                row.value = userProfile?.firstName ?? ""
                row.add(rule: RuleRequired())
                row.disabled = true
            }
            <<< NameRow(Constants.Keys.LastNameKey){ row in
                row.title = "Last Name"
                row.value = userProfile?.lastName ?? ""
                row.add(rule: RuleRequired())
                row.disabled = true
            }
            <<< PickerInputRow<String>(Constants.Keys.GenderKey){
                $0.title = "Gender"
                $0.options = Constants.AvailableGenderLevels
                $0.value = userProfile?.gender ?? $0.options.first
                $0.add(rule: RuleRequired())
                $0.disabled = true
        }
        form +++ Section("Additional Info")
            <<< DateRow(Constants.Keys.BirthdayKey){ row in
                row.title = "Birthday"
                row.value = userProfile?.birthday ?? Date()
                let formatter = DateFormatter()
                formatter.locale = .current
                formatter.dateStyle = .long
                row.dateFormatter = formatter
                row.disabled = true
            }
            <<< PickerInputRow<String>(Constants.Keys.EducationKey) {
                $0.title = "Education"
                $0.options = Constants.AvailableEducationLevels
                $0.value = userProfile?.education ?? $0.options.first
                $0.disabled = true
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



