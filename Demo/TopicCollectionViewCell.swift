//
//  TopicCollectionViewCell.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit

class TopicCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
}
