//
//  NewTopicViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/6/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import ChameleonFramework
import Segmentio
import SkyFloatingLabelTextField
import Firebase

class NewTopicViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var groupSegmenter: Segmentio!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var topicName: SkyFloatingLabelTextField!
    @IBOutlet weak var questionName: SkyFloatingLabelTextField!
    @IBOutlet weak var groupName: SkyFloatingLabelTextField!
    @IBOutlet weak var groupDescription: SkyFloatingLabelTextField!
    
    var groupOne: ChatGroup!
    var groupTwo: ChatGroup!
    var battlezone: ChatGroup!
    
    var currentTopicRef: DatabaseReference!
    var currentTopicName: String!
    var currentGroupIndex = 0
    
    var groupNames = ["","",""]
    var groupDescriptions = ["","",""]
    
    fileprivate func setupGroupSegmenter() {
        let groupLeft = SegmentioItem(title: "Group 1", image: nil)
        let groupRight = SegmentioItem(title: "Group 2", image: nil)
        let battlezone = SegmentioItem(title: "Battlezone", image: nil)
        let segmenterStates = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            selectedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            highlightedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            )
        )
        let segmenterOptions = SegmentioOptions(
            backgroundColor: .white,
            maxVisibleItems: 3,
            scrollEnabled: false,
            indicatorOptions: SegmentioIndicatorOptions(
                type: .bottom,
                ratio: 1,
                height: 2,
                color: FlatSkyBlue()),
            horizontalSeparatorOptions: SegmentioHorizontalSeparatorOptions(
                type: .bottom,
                height: 1,
                color: .gray),
            verticalSeparatorOptions: SegmentioVerticalSeparatorOptions(
                ratio: 0.6,
                color: .clear),
            imageContentMode: .center,
            labelTextAlignment: .center,
            labelTextNumberOfLines: 1,
            segmentStates: segmenterStates,
            animationDuration: 0.3)
        groupSegmenter.setup(content: [groupLeft, groupRight, battlezone], style: .onlyLabel, options: segmenterOptions)
        groupSegmenter.selectedSegmentioIndex = 0
        
        groupSegmenter.valueDidChange = { _, segmentIndex in
            if let name = self.groupName.text {
                self.groupNames[self.currentGroupIndex] = name
            }
            if let description  = self.groupDescription.text {
                self.groupDescriptions[self.currentGroupIndex] = description
            }
            self.currentGroupIndex = segmentIndex
            self.groupName.text = self.groupNames[segmentIndex]
            self.groupDescription.text = self.groupDescriptions[segmentIndex]
        }
    }
    
    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGroupSegmenter()
        
        if currentTopicName != nil {
            topicName.text = currentTopicName
            topicName.isUserInteractionEnabled = false
        }
        
        // used for hiding keyboard control
        topicName.delegate = self
        questionName.delegate = self
        groupName.delegate = self
        groupDescription.delegate = self
        
        createButton.layer.cornerRadius = 20
        
        setupKeyboardObservers()
    }
    
    fileprivate func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeKeyboardObservers()
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = -keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    

    @IBAction func createTopicAction() {
        if checkFillup() {
            let groupIDs = [randomString(length: 64), randomString(length: 64), randomString(length: 64)]
            for i in 0...2 {
                let singleGroupData = ["name": groupNames[i], "description": groupDescriptions[i], "id": groupIDs[i]]
                let ref = Constants.refs.databaseGroups.child(singleGroupData["id"]!)
                ref.setValue(singleGroupData)
            }
            if currentTopicRef == nil {
                let topicData = ["name": topicName.text!, "id": randomString(length: 64)]
                currentTopicRef = Constants.refs.databaseTopics.child(topicData["id"]!)
                let topicMetadata = currentTopicRef.child("metadata")
                topicMetadata.setValue(topicData)
            }
            
            let questionRef = currentTopicRef.child("questions").childByAutoId()
            let questionData: [String: Any] = ["name": questionName.text!, "groupIDs": groupIDs, "conclusionOne": "TBD", "conclusionTwo": "TBD", "ownerID": UserDefaults.standard.value(forKey: Constants.Keys.UsernameKey) as! String]
            
            questionRef.setValue(questionData)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func checkFillup() -> Bool {
        if topicName.text == ""  {
            topicName.lineColor = FlatRed()
            return false
        }
        if questionName.text == "" {
            questionName.lineColor = FlatRed()
            return false
        }
        groupSegmenter.selectedSegmentioIndex = 2 - currentGroupIndex
        for i in 0...2 {
            if groupNames[i] == "" {
                groupSegmenter.selectedSegmentioIndex = i
                groupName.lineColor = FlatRed()
                return false
            }
            if groupDescriptions[i] == "" {
                groupSegmenter.selectedSegmentioIndex = i
                groupDescription.lineColor = FlatRed()
                return false
            }
        }
        return true
        
    }
    
    @IBAction func cancelAction() {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
