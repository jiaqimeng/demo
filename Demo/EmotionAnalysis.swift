//
//  EmotionAnalysis.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/6/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class EmotionAnalysis {
    let session = URLSession.shared
    var url = URL(string: Constants.GoogleNLPSentimenturl)
    
    func createRequest(text: String, handler: @escaping (Float)->Void) {
        
        let jsonForm: [String:Any] = [
            "document": [
                "type": "PLAIN_TEXT",
                "content": text],
            "encodingType": "UTF8"
        ]
        
        Alamofire.request(Constants.GoogleNLPSentimenturl, method: .post, parameters: jsonForm, encoding: JSONEncoding.default).responseJSON { response in
            if let rawVal = response.result.value {
                let jsonVal = JSON(rawVal)
                if let sentimentScore = jsonVal["documentSentiment"]["score"].float {
                    handler(sentimentScore)
                }
                print(jsonVal)
                
            }
        }
        
    }
}
