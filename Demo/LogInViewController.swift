//
//  LogInViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Firebase
import FirebaseAuth
import ChameleonFramework
import NVActivityIndicatorView

class LogInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    let defaults = UserDefaults.standard
    
    var topics: [TopicModel]!
    private var blurEffect: UIVisualEffectView!
    private var pendingIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pendingIndicator = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.size.width*0.5-25, y: UIScreen.main.bounds.size.height*0.5-25, width: 50, height: 50), type: NVActivityIndicatorType.ballRotate)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Authenticating...")
        self.blurEffect = blurEffectView(view: view)
        usernameField.placeholder = "Username/Email"
        usernameField.title = "Your username/email"
        passwordField.placeholder = "Password"
        passwordField.title = "Your password"
        
        usernameField.delegate = self
        passwordField.delegate = self
        signInButton.layer.cornerRadius = 10
        signUpButton.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupKeyboardObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let lastUserName = self.defaults.value(forKey: Constants.Keys.UsernameKey) as? String, let lastPassword = self.defaults.value(forKey: Constants.Keys.PasswordKey) as? String {
            usernameField.text = lastUserName
            passwordField.text = lastPassword
        
            signInButton.sendActions(for: .touchUpInside)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    
    fileprivate func pendingIndicatorOn() {
        self.view.addSubview(self.blurEffect)
        self.view.addSubview(self.pendingIndicator)
        self.pendingIndicator.startAnimating()
    }
    
    fileprivate func pendingIndicatorOff() {
        self.blurEffect.removeFromSuperview()
        self.pendingIndicator.removeFromSuperview()
        self.pendingIndicator.stopAnimating()
    }
    
    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -20

    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @IBAction func SignInAction() {
        self.view.endEditing(true)
        if usernameField.text == "" || passwordField.text == "" {

            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in

            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)

            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)

        } else {
            pendingIndicatorOn()
            Auth.auth().signIn(withEmail: self.usernameField.text!, password: passwordField.text!) { (user, error) in
                if error == nil {
                    self.defaults.setValue(self.usernameField.text, forKey: Constants.Keys.UsernameKey)
                    self.defaults.setValue(self.passwordField.text, forKey: Constants.Keys.PasswordKey)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarHome")
                    fetchProfile(userNameHash: self.usernameField.text!.md5()) {
                        self.present(vc!, animated: true, completion: nil)
                        self.pendingIndicatorOff()
                    }

                } else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)

                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    self.pendingIndicatorOff()
                }
                
            }
        }
    }
    
    
    
    
    
//    @IBAction func SignUpAction() {
//        self.view.endEditing(true)
//        if usernameField.text == "" {
//            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
//
//            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//        } else {
//            pendingIndicatorOn()
//            Auth.auth().createUser(withEmail: usernameField.text!, password: passwordField.text!) { (user, error) in
//
//                if error == nil {
//                    self.defaults.setValue(self.usernameField.text, forKey: Constants.Keys.UsernameKey)
//                    self.defaults.setValue(self.passwordField.text, forKey: Constants.Keys.PasswordKey)
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarHome") as! UITabBarController
//                    self.present(vc, animated: true, completion: nil)
//
//                } else {
//                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
//
//                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//                    alertController.addAction(defaultAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//                }
//                self.pendingIndicatorOff()
//            }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "JumpToSignUpPage") {
//            let vc = segue.destination as! SignUpViewController
//        }
//    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}


