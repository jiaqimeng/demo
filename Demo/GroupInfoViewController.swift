//
//  GroupInfoViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 12/4/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import ChameleonFramework

class GroupInfoViewController: FormViewController {
    
    var chatGroup: ChatGroup!
    var locked: Bool!
    var lockMessage: String!
    var sensitivity: Float!
    var disableEdit: Condition = false
    var disableBool: Bool = false
    
    fileprivate func setupForm() {
        CheckRow.defaultCellSetup = { cell, row in cell.tintColor = FlatRed()}
        
        if let ownerID = chatGroup.ownerID {
            if ownerID != (UserDefaults.standard.value(forKey: Constants.Keys.UsernameKey) as! String) {
                disableEdit = true
                disableBool = true
            }
        }
        
        form +++ Section("GroupName")
            <<< NameRow(Constants.Keys.GroupNameKey){ row in
                row.value = chatGroup?.groupName
                row.add(rule: RuleRequired())
                row.disabled = disableEdit
            }
        form +++ Section("Decribe this group")
            <<< TextAreaRow(Constants.Keys.GroupDescriptionKey){ row in
                row.value = chatGroup.groupDescription
                row.add(rule: RuleRequired())
                row.disabled = disableEdit
            }
        form +++ Section("Conclusion summary")
            <<< TextAreaRow(Constants.Keys.GroupConclusionKey){ row in
                row.placeholder = "Write a summary of conclusions"
                row.value = chatGroup.conclusion
                row.add(rule: RuleRequired())
                row.disabled = disableEdit
                
            }
        form +++ Section("Sentiment Detection Sensitivity")
            <<< SliderRow(Constants.Keys.SentimentSensitivity){ row in
                row.maximumValue = 0.5
                row.minimumValue = -0.5
                row.value = sensitivity
                row.steps = 10
                row.cell.slider.isContinuous = true
                row.disabled = disableEdit
            }
            
        form +++ Section("Group Control")
            
            <<< CheckRow(Constants.Keys.GroupLockKey){ row in
                row.title = "Red Lock"
                row.value = locked
                row.disabled = disableEdit
        }
            <<< TextAreaRow(Constants.Keys.GroupLockMessageKey){ row in
                row.placeholder = "Add a lock explanation (optional)"
                row.value = lockMessage
                row.disabled = disableEdit
        }
    }
    
    func setupAlert() {
        if disableBool {
            let alert = UIAlertController(title: "Are you an owner?", message: "Oops! It seems you are not the owner of this group! Edit feature are disabled.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got it!", style: .default, handler: { _ in
            }))
            
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupForm()
        setupAlert()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let errors = form.validate()
        if errors.count != 0 {
            return
        }
        var updatedGroupInfo = [String: String]()
        var updatedConclusion = [String: String]()
        var updatedLockInfo = [String: Any]()
        let groupNameRow = form.rowBy(tag: Constants.Keys.GroupNameKey) as! NameRow
        updatedGroupInfo[Constants.Keys.GroupNameKey] = groupNameRow.value!
        let groupDescriptionRow = form.rowBy(tag: Constants.Keys.GroupDescriptionKey) as! TextAreaRow
        updatedGroupInfo[Constants.Keys.GroupDescriptionKey] = groupDescriptionRow.value!
        Constants.refs.databaseGroups.child(chatGroup.groupID).updateChildValues(updatedGroupInfo)
        
        let groupConclusionRow = form.rowBy(tag: Constants.Keys.GroupConclusionKey) as! TextAreaRow
        
        updatedConclusion[Constants.Keys.conclusionIndexKeys[chatGroup.groupIndex]] = groupConclusionRow.value!
        Constants.refs.databaseTopics.child(chatGroup.topicID).child("questions").child(chatGroup.questionID).updateChildValues(updatedConclusion)
        
        let lockGroupRow = form.rowBy(tag: Constants.Keys.GroupLockKey) as! CheckRow
        let lockMessageRow = form.rowBy(tag: Constants.Keys.GroupLockMessageKey) as! TextAreaRow
        let sensitivityRow = form.rowBy(tag: Constants.Keys.SentimentSensitivity) as! SliderRow
        updatedLockInfo[Constants.Keys.GroupLockKey] = lockGroupRow.value!
        updatedLockInfo[Constants.Keys.GroupLockMessageKey] = lockMessageRow.value!
        updatedLockInfo[Constants.Keys.SentimentSensitivity] = sensitivityRow.value!
        Constants.refs.databaseChats.child(chatGroup.groupID).child("lock").setValue(updatedLockInfo)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
