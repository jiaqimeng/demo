//
//  Group.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import Foundation

class ChatGroup {
    var groupName: String
    var groupIndex: Int
    var groupDescription: String
    var groupID: String
    var questionID: String
    var topicID: String
    var numParticipants: Int?
    var conclusion: String?
    var ownerID: String?
    
    init(name: String, description: String, id: String, parentQuestionID: String, topicID: String, groupIndex: Int) {
        groupName = name
        groupDescription = description
        groupID = id
        questionID = parentQuestionID
        self.groupIndex = groupIndex
        self.topicID = topicID
    }
    
    func setNumParticipants(numParticipants: Int) {
        self.numParticipants = numParticipants
    }
    
    func setConclusion(conclusion: String) {
        self.conclusion = conclusion
    }
    
    func setOwnerID(ownerID: String?) {
        self.ownerID = ownerID
    }
}
