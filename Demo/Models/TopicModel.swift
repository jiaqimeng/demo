//
//  TopicModel.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import Foundation

class TopicModel {
    
    var topicName: String
    var topicID: String
    var hotness: String?
    var backgroundImage: UIImage?
    var questions: [QuestionModel]
    
    init(topicName: String, questions: [QuestionModel], id: String) {
        self.topicName = topicName
        self.questions = questions
        self.topicID = id
    }
}
