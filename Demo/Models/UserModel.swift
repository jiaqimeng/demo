//
//  UserModel.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/25/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import Foundation
import UIKit

class UserModel: NSObject, NSCoding {
    var profileImage: UIImage?
    var firstName: String
    var lastName: String
    var gender: String
    var birthday: Date?
    var education: String?
    
    init(firstName: String, lastName: String, gender: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.gender = gender
    }
    
    func setBirthday(birthday: Date) {
        self.birthday = birthday
    }
    
    func setProfileImage(image: UIImage) {
        self.profileImage = image
    }
    
    func setEducation(education: String) {
        self.education = education
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let firstName = aDecoder.decodeObject(forKey: Constants.Keys.FirstNameKey) as! String
        let lastName = aDecoder.decodeObject(forKey: Constants.Keys.LastNameKey) as! String
        let gender = aDecoder.decodeObject(forKey: Constants.Keys.GenderKey) as! String
        self.init(firstName: firstName, lastName: lastName, gender: gender)
        if let birthday = aDecoder.decodeObject(forKey: Constants.Keys.BirthdayKey) as? Date {
            self.setBirthday(birthday: birthday)
        }
        if let education = aDecoder.decodeObject(forKey: Constants.Keys.EducationKey) as? String {
            self.setEducation(education: education)
        }
        if let profileImage = aDecoder.decodeObject(forKey: Constants.Keys.MyProfileImageKey) as? UIImage {
            self.setProfileImage(image: profileImage)
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: Constants.Keys.FirstNameKey)
        aCoder.encode(lastName, forKey: Constants.Keys.LastNameKey)
        aCoder.encode(gender, forKey: Constants.Keys.GenderKey)
        if birthday != nil {
            aCoder.encode(birthday, forKey: Constants.Keys.BirthdayKey)
        }
        if education != nil {
            aCoder.encode(education, forKey: Constants.Keys.EducationKey)
        }
        if profileImage != nil {
            aCoder.encode(profileImage, forKey: Constants.Keys.MyProfileImageKey)
        }
    }
    
}
