//
//  QuestionModel.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import Foundation


class QuestionModel {
    var questionName: String
    var id: String
    var topicID: String
    var conclusionLeft: String
    var conclusionRight: String
    var groupIDs: [String]!
    var groupLeft: ChatGroup!
    var groupRight: ChatGroup!
    var battleground: ChatGroup!
    var ownerID: String?
    
    init(question: String, conclusionLeft: String, conclusionRight: String, id: String, parentTopicID: String) {
        self.id = id
        self.topicID = parentTopicID
        questionName = question
        self.conclusionLeft = conclusionLeft
        self.conclusionRight = conclusionRight
    }
    
    func setGroupLeft(group: ChatGroup) -> QuestionModel {
        self.groupLeft = group
        return self
    }
    
    func setGroupRight(group: ChatGroup) -> QuestionModel{
        self.groupRight = group
        return self
    }
    
    func setBattleground(group: ChatGroup) -> QuestionModel{
        self.battleground = group
        return self
    }
    
    func setGroupIDs(IDs: [String]) -> QuestionModel {
        self.groupIDs = IDs
        return self
    }
    
    func setOwnerID(ownerID: String?) {
        self.ownerID = ownerID
    }
}
