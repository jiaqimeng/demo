//
//  ProfileViewTableViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/18/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView
import ChameleonFramework

class ProfileViewTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profileImageCell: UITableViewCell!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    let imagePicker = UIImagePickerController()
    let userdefaults = UserDefaults.standard
    let hashedUsername = (UserDefaults.standard.value(forKey: Constants.Keys.UsernameKey) as! String).md5()
    var userProfile: UserModel? {
        didSet {
            self.profileImage.image = userProfile!.profileImage
            self.profileName.text = "\(userProfile!.firstName) \(userProfile!.lastName)"
        }
    }
    var shouldFetchProfile = false
    
    fileprivate func prepareProfileImage() {
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    fileprivate func extractUserFromDefaults() {
        if let userProfileEncodeddata = self.userdefaults.value(forKey: Constants.Keys.MyProfileKey) as? Data {
            let userProfile = NSKeyedUnarchiver.unarchiveObject(with: userProfileEncodeddata) as! UserModel
            self.userProfile = userProfile
        }
    }
    
    fileprivate func setProfileImage() {
        if shouldFetchProfile {
            fetchProfile(userNameHash: hashedUsername) {
                self.extractUserFromDefaults()
                self.shouldFetchProfile = false
            }
        } else {
            self.extractUserFromDefaults()
        }
    }
    
    fileprivate func encodeAndSaveProfileImage(_ userModel: UserModel) {
        let encodedProfileData = NSKeyedArchiver.archivedData(withRootObject: userModel)
        self.userdefaults.setValue(encodedProfileData, forKey: Constants.Keys.MyProfileKey)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareProfileImage()
        imagePicker.delegate = self
        tableView.tableFooterView = UIView()
        profileImageCell.backgroundColor = GradientColor(.diagonal, frame: profileImageCell.frame, colors: [RandomFlatColor(), RandomFlatColor()])
        profileName.textColor = ContrastColorOf(profileImageCell.backgroundColor!, returnFlat: true)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setProfileImage()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        // Your action
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.shouldFetchProfile = true
        let pendingIndicator = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.size.width*0.5-25, y: UIScreen.main.bounds.size.height*0.5-25, width: 50, height: 50), type: NVActivityIndicatorType.ballRotate)
        let blurEffect = blurEffectView(view: view)
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            profileImage.image = pickedImage
            if let profile = userProfile {
                profile.setProfileImage(image: pickedImage)
                let encodedProfileData = NSKeyedArchiver.archivedData(withRootObject: profile)
                userdefaults.setValue(encodedProfileData, forKey: Constants.Keys.MyProfileKey)
            }
            
            let imageData = UIImageJPEGRepresentation(pickedImage, 0.7)!
            Constants.refs.storageImages.child(hashedUsername).putData(imageData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    print(error)
                    return
                }
                if let profileImageURL = metadata?.downloadURL()?.absoluteString {
                    let profileRef = Constants.refs.databaseUsers.child(self.hashedUsername)
                    profileRef.updateChildValues(["profileImageURL": profileImageURL])
                }
                blurEffect.removeFromSuperview()
                pendingIndicator.stopAnimating()
                
                self.dismiss(animated: true, completion: nil)
            })
            
        }
        picker.view.addSubview(blurEffect)
        picker.view.addSubview(pendingIndicator)
        pendingIndicator.startAnimating()
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        }
        return 5
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileEditVCSegue" {
            if let profileEditVC = segue.destination as? ProfileEditViewController {
                profileEditVC.userProfile = userProfile
                shouldFetchProfile = true
            }
        }
    }

//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
//
//        return cell
//    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
