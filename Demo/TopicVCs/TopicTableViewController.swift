//
//  TopicTableViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import ChameleonFramework
import FirebaseDatabase

class TopicTableViewController: UITableViewController {
    
    var topics: [TopicModel] = []
    var currentSelect = 0
    var parentVC: HomeViewController!

    @IBAction func logoutAction() {
        UserDefaults.standard.removeObject(forKey: Constants.Keys.UsernameKey)
        UserDefaults.standard.removeObject(forKey: Constants.Keys.MyProfileKey)
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = true
        self.refreshControl?.beginRefreshing()
        fetchNewTopics { newTopics in
            self.topics = newTopics
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        
        fetchNewTopics { newTopics in
            self.topics = newTopics
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        }
    }
    
    func fetchNewTopics(completion: @escaping (_ tableData:[TopicModel]) -> ()) {
        let topicRef = Constants.refs.databaseTopics
        
        topicRef.observe(.value) { snapshot in
            var newTopics: [TopicModel] = []
            for childTopic in snapshot.children {
                let childTopicSnapshot = childTopic as! DataSnapshot
                let questionSnapshots = childTopicSnapshot.childSnapshot(forPath: "questions")
                let metadataSnapshots = childTopicSnapshot.childSnapshot(forPath: "metadata")
                var questionModels: [QuestionModel] = []
                
                for questionSnapshot in questionSnapshots.children {
                    let firSnapshot = questionSnapshot as! DataSnapshot
                    if  let questionData = firSnapshot.value as? [String: Any],
                        let questionName = questionData["name"] as? String,
                        let groupIDs = questionData["groupIDs"] as? [String]
                    {
                        let conclusionOne = questionData["conclusionOne"] as? String ?? "TBD"
                        let conclusionTwo = questionData["conclusionTwo"] as? String ?? "TBD"
                        let questionModel = QuestionModel(question: questionName, conclusionLeft: conclusionOne, conclusionRight: conclusionTwo, id: firSnapshot.key, parentTopicID: childTopicSnapshot.key)
                        if let ownerID = questionData["ownerID"] as? String {
                            questionModel.setOwnerID(ownerID: ownerID)
                        }
                        questionModel.setGroupIDs(IDs: groupIDs)
                        questionModels.append(questionModel)
                    }
                }
                if let metadata = metadataSnapshots.value as? [String: String],
                    let topicName = metadata["name"],
                    let topicID = metadata["id"] {
                    let topicModel = TopicModel(topicName: topicName, questions: questionModels, id: topicID)
                    newTopics.append(topicModel)
                }
            }
            topicRef.removeAllObservers()
            completion(newTopics)
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return topics.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicCellReuseID", for: indexPath)
        if let topicVTCell = cell as? TopicTableViewCell {
            let topic = topics[indexPath.row]
            if let img = topic.backgroundImage {
                topicVTCell.backgroundImage.image = img
            }
            topicVTCell.topicName.text = topic.topicName
            let randomNumber = arc4random_uniform(10)
            if topicVTCell.backgroundImage.image == nil {
                topicVTCell.backgroundImage.image = UIImage(named: "\(randomNumber)")
            }
            return topicVTCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelect = indexPath.row
        performSegue(withIdentifier: "ToQuestionTVSegue", sender: self)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let questionVC = segue.destination as? QuestionViewController, segue.identifier == "ToQuestionTVSegue" {
            questionVC.topicModel = topics[currentSelect]
            questionVC.parentVC = parentVC
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
