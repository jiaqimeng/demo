//
//  QuestionTableViewCell.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import FaveButton

class QuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var likeNumberTwo: UILabel!
    @IBOutlet weak var likeNumberOne: UILabel!
    @IBOutlet weak var questionName: UILabel!
    @IBOutlet weak var conclusionOne: UILabel!
    @IBOutlet weak var ConclusionTwo: UILabel!
    @IBOutlet weak var joinGroupButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        joinGroupButton.layer.cornerRadius = 20
        // Initialization code
        let randomNumber1 = arc4random_uniform(10)
        let randomNumber2 = arc4random_uniform(10)
        self.likeNumberTwo.text = String(randomNumber1)
        self.likeNumberOne.text = String(randomNumber2)
    }
    var numberOneStatus = false
    var numberTwoStatus = false
    
    @IBAction func buttonOneAction(_ sender: Any) {
        if(numberOneStatus == false) {
            let num = Int(self.likeNumberOne.text!)
            self.likeNumberOne.text = String(num! + 1)
            numberOneStatus = true
        } else {
            let num = Int(self.likeNumberOne.text!)
            self.likeNumberOne.text = String(num! - 1)
            numberOneStatus = false
        }
    }
    
    @IBAction func buttonTwoAction(_ sender: Any) {
        if(numberTwoStatus == false) {
            let num = Int(self.likeNumberTwo.text!)
            self.likeNumberTwo.text = String(num! + 1)
            numberTwoStatus = true
        } else {
            let num = Int(self.likeNumberTwo.text!)
            self.likeNumberTwo.text = String(num! - 1)
            numberTwoStatus = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
