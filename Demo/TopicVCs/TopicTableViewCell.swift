//
//  TopicTableViewCell.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var topicName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        topicName.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        topicName.layer.cornerRadius = 10
        topicName.clipsToBounds = true
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.clipsToBounds = true
        backgroundImage.layer.cornerRadius = 20
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
