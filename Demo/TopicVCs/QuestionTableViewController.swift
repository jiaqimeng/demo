//
//  QuestionTableViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import FirebaseDatabase

class QuestionTableViewController: UITableViewController {
    
    var questionModels: [QuestionModel]!
    var currentSelect = 0
    var parentVC: HomeViewController!
    var topicID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)
        self.tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        
        fetchNewQuestions { newQuestions in
            self.questionModels = newQuestions
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        }
    }
    
    func fetchNewQuestions(completion: @escaping (_ tableData:[QuestionModel]) -> ()) {
        let questionRef = Constants.refs.databaseTopics.child(topicID).child("questions")
        
        questionRef.observe(.value) { snapshot in
            var newQuestions: [QuestionModel] = []
            for childQuestion in snapshot.children {
                let childQuestionSnapshot = childQuestion as! DataSnapshot
                if  let questionData = childQuestionSnapshot.value as? [String: Any],
                    let questionName = questionData["name"] as? String,
                    let groupIDs = questionData["groupIDs"] as? [String]
                    
                {
                    let conclusionOne = questionData["conclusionOne"] as? String ?? "TBD"
                    let conclusionTwo = questionData["conclusionTwo"] as? String ?? "TBD"
                    let questionModel = QuestionModel(question: questionName, conclusionLeft: conclusionOne, conclusionRight: conclusionTwo, id: childQuestionSnapshot.key, parentTopicID: self.topicID)
                    questionModel.setGroupIDs(IDs: groupIDs)
                    newQuestions.append(questionModel)
                    if let ownerID = questionData["ownerID"] as? String {
                        questionModel.setOwnerID(ownerID: ownerID)
                    }
                }
            }
            
            completion(newQuestions)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            parentVC.addTopicButton.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return questionModels.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionTVCReuseID", for: indexPath)
        
        if let questionTVC = cell as? QuestionTableViewCell {
            questionTVC.joinGroupButton.addTarget(self, action: #selector(cellButtonAction), for: .touchUpInside)
            let question = questionModels[indexPath.row]
            questionTVC.questionName.text = question.questionName
            questionTVC.conclusionOne.text = question.conclusionLeft
            questionTVC.ConclusionTwo.text = question.conclusionRight
            return questionTVC
        }

        return cell
    }
    
    @objc func cellButtonAction(_ button: UIButton) {
        playSelectGroupSegue(button.tag)
    }
    
    fileprivate func playSelectGroupSegue(_ row: Int) {
        currentSelect = row
        performSegue(withIdentifier: "SelectGroupSegue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        playSelectGroupSegue(indexPath.row)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SelectGroupViewController, segue.identifier == "SelectGroupSegue" {
            
            let question = questionModels[currentSelect]
            fetchGroups(question: question, completion: { questionWithGroups in
                vc.question = questionWithGroups
                self.parentVC.addTopicButton.isHidden = true
            })
        }
    }
    
    func fetchGroups(question: QuestionModel, completion: @escaping (QuestionModel) -> Void) {
        let dispatchGroup = DispatchGroup()
        for i in 0...2 {
            dispatchGroup.enter()
            let groupRef = Constants.refs.databaseGroups.child(question.groupIDs[i])
            groupRef.observe(.value) { snapshot in
                if let data = snapshot.value as? [String: String],
                    let description = data["description"],
                    let name = data["name"],
                    let id = data["id"] {
                    if i == 0 {
                        question.groupLeft = ChatGroup(name: name, description: description, id: id, parentQuestionID: question.id, topicID: self.topicID, groupIndex: i)
                        question.groupLeft.setConclusion(conclusion: question.conclusionLeft)
                        question.groupLeft.setOwnerID(ownerID: question.ownerID)
                    }
                    else if i == 1 {
                        question.groupRight = ChatGroup(name: name, description: description, id: id, parentQuestionID: question.id, topicID: self.topicID, groupIndex: i)
                        question.groupRight.setConclusion(conclusion: question.conclusionRight)
                        question.groupRight.setOwnerID(ownerID: question.ownerID)
                    } else {
                        question.battleground = ChatGroup(name: name, description: description, id: id, parentQuestionID: question.id, topicID: self.topicID, groupIndex: i)
                        question.battleground.setOwnerID(ownerID: question.ownerID)
                    }
                    
                    
                }
                groupRef.removeAllObservers()
                dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            completion(question)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
