//
//  SignUpViewController.swift
//  Demo
//
//  Created by Michael Zhang on 11/25/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Firebase
import FirebaseAuth
import ChameleonFramework
import NVActivityIndicatorView


class SignUpViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var repeatPasswordField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var genderField: SkyFloatingLabelTextField!
    @IBOutlet weak var educationField: SkyFloatingLabelTextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    let defaults = UserDefaults.standard
    var topics: [TopicModel]!
    
    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func setupGenderTextField() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        genderField.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.tintColor = FlatBlue()
        toolBar.sizeToFit()
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        toolBar.setItems([spaceItem, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        genderField.inputAccessoryView = toolBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareProfileImage()
        setupGenderTextField()
        signUpButton.layer.cornerRadius = 10
        
        imagePicker.delegate = self
        profileImage.image = UIImage(named: "cat")
        usernameField.delegate = self
        passwordField.delegate = self
        repeatPasswordField.delegate = self
        firstNameField.delegate = self
        lastNameField.delegate = self
        educationField.delegate = self
        signUpButton.layer.cornerRadius = 10
        setupKeyboardObservers()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func prepareProfileImage() {
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObservers()
    }
    
    @objc func donePicker(sender: UIBarButtonItem) {
        genderField.resignFirstResponder()
    }

    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        // Your action
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -20
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @IBAction func SignUpAction() {
        if usernameField.text == "" || passwordField.text == "" || firstNameField.text == "" || lastNameField.text == "" || genderField.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter your email, password, full name, and gender", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            let pendingIndicator = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.size.width*0.5-25, y: UIScreen.main.bounds.size.height*0.5-25, width: 50, height: 50), type: NVActivityIndicatorType.ballRotate)
            let blurEffect = blurEffectView(view: view)
            self.view.addSubview(blurEffect)
            self.view.addSubview(pendingIndicator)
            pendingIndicator.startAnimating()
            Auth.auth().createUser(withEmail: usernameField.text!, password: passwordField.text!) { (user, error) in
                if error == nil {
                    let username = self.usernameField.text!
                    let hashedUsername = username.md5()
                    var updatedUserInfo = [String: String]()
                    updatedUserInfo[Constants.Keys.FirstNameKey] = self.firstNameField.text!
                    updatedUserInfo[Constants.Keys.LastNameKey] = self.lastNameField.text!
                    updatedUserInfo[Constants.Keys.GenderKey] = self.genderField.text!
                    let userModel = UserModel(firstName: self.firstNameField.text!, lastName: self.lastNameField.text!, gender: self.genderField.text!)
                    if let education =  self.educationField.text {
                        updatedUserInfo[Constants.Keys.EducationKey] = education
                        userModel.setEducation(education: education)
                    }
                    
                    Constants.refs.databaseUsers.child(hashedUsername).updateChildValues(updatedUserInfo)
            
                    let imageData = UIImageJPEGRepresentation(self.profileImage.image!, 0.7)!
                    Constants.refs.storageImages.child(hashedUsername).putData(imageData, metadata: nil, completion: { (metadata, error) in
                        if error != nil {
                            print(error)
                            return
                        }
                        if let profileImageURL = metadata?.downloadURL()?.absoluteString {
                            let profileRef = Constants.refs.databaseUsers.child(hashedUsername)
                            profileRef.updateChildValues([Constants.Keys.ProfileImageURLKey: profileImageURL])
                        }
                        blurEffect.removeFromSuperview()
                        pendingIndicator.stopAnimating()
                        userModel.setProfileImage(image: self.profileImage.image!)
                        encodeAndSaveToCache(userModel)
                        self.dismiss(animated: true, completion: nil)
                        self.defaults.setValue(self.usernameField.text, forKey: Constants.Keys.UsernameKey)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarHome")
                        self.present(vc!, animated: true, completion: nil)
                    })
                }
                
                else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
  
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            profileImage.image = pickedImage
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelSignUp() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Constants.AvailableGenderLevels.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Constants.AvailableGenderLevels[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderField.text = Constants.AvailableGenderLevels[row]
    }
    
}
