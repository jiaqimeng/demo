//
//  ImageCell.swift
//  Demo
//
//  Created by Meng Jiaqi on 12/6/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Eureka

class ImageCell: Cell<UserModel>, CellType {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
