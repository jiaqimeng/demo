//
//  HomeViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var addTopicButton: UIButton!
    var childNavigationVC: UINavigationController!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navigationVC = segue.destination as? UINavigationController, segue.identifier == "TopicTableVCSegue" {
            let topicTVC = navigationVC.childViewControllers[0] as! TopicTableViewController
            childNavigationVC = navigationVC
            topicTVC.parentVC = self
            
        }
        if let newTopicVC = segue.destination as? NewTopicViewController {
            if childNavigationVC.childViewControllers.count == 2 {
                // in question page
                if let questionVC = childNavigationVC.childViewControllers[1] as? QuestionViewController {
                    newTopicVC.currentTopicRef = Constants.refs.databaseTopics.child(questionVC.topicModel.topicID)
                    newTopicVC.currentTopicName = questionVC.topicModel.topicName
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func newTopicAction() {
    }
    
}
