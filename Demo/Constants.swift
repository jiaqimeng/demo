//
//  Constants.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//
import Foundation
import Firebase
import UIKit
import CryptoSwift

struct Constants {
    struct refs {
        static let databaseRoot = Database.database().reference()
        static let databaseGroups = databaseRoot.child("groups")
        static let databaseChats = databaseRoot.child("chats")
        static let databaseTopics = databaseRoot.child("topics")
        static let databaseQuestion = databaseTopics.child("questions")
        static let databaseUsers = databaseRoot.child("users")
        
        static let storage = Storage.storage()
        static let storageRoot = storage.reference()
        static let storageImages = storageRoot.child("images")
        
    }
    
    struct Keys {
        static let FirstNameKey = "FirstName"
        static let LastNameKey = "LastName"
        static let GenderKey = "Gender"
        static let BirthdayKey = "Birthday"
        static let EducationKey = "Education"
        static let UsernameKey = "username"
        static let MyProfileKey = "myProfile"
        static let MyProfileImageKey = "myProfileImage"
        static let ProfileImageURLKey = "profileImageURL"
        static let LastTimeFetchProfile = "lastTimeFetchProfile"
        static let PasswordKey = "password"
        static let GroupNameKey = "name"
        static let GroupDescriptionKey = "description"
        static let NumParticipantsKey = "groupNumberOfParticipants"
        static let GroupConclusionKey = "groupConclusion"
        static let conclusionIndexKeys = ["conclusionOne", "conclusionTwo", "conclusionBattlezone"]
        static let GroupLockKey = "groupLock"
        static let GroupLockMessageKey = "groupLockMessage"
        static let SentimentSensitivity = "senstimentSensitivity"
    }
    
    struct utils {
        var dateFormatter: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            return formatter
        }
    }
    
    static let GoogleAPIK = "AIzaSyCdIYHPyy5UMndmBlQZ6LLPDuT8RTJqaww"
    static let GoogleNLPSentimenturl = "https://language.googleapis.com/v1/documents:analyzeSentiment?key=AIzaSyCdIYHPyy5UMndmBlQZ6LLPDuT8RTJqaww"
    static let RandomStringLength = 128
    static let topicIDBroadcastKey = "com.JM.currentTopicIDBroadcastKey"
    static let GroupNameAttributes = [ NSAttributedStringKey.font: UIFont(name: "Aventir-Heavy", size: 18)]
    static let GroupDescriptionAttributes = [ NSAttributedStringKey.font: UIFont(name: "Aventir-Book", size: 18)]
    static let CountryCodes = NSLocale.isoCountryCodes
    static let AvailableEducationLevels = ["None", "Primary School", "Middle School", "College", "Unspecified"]
    static let AvailableGenderLevels = ["Male", "Female", "Other"]
}


func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}

func blurEffectView(view: UIView) -> UIVisualEffectView {
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = view.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    return blurEffectView
}

func encodeAndSaveToCache(_ userModel: UserModel) {
    let encodedProfileData = NSKeyedArchiver.archivedData(withRootObject: userModel)
    UserDefaults.standard.setValue(encodedProfileData, forKey: Constants.Keys.MyProfileKey)
}


func fetchProfile(userNameHash: String, completion: @escaping () -> Void) {
    let profileRef = Constants.refs.databaseUsers.child(userNameHash)
    
    profileRef.observe(.value) { snapshot in
        if let profileData = snapshot.value as? [String: String] {
            if let firstName = profileData[Constants.Keys.FirstNameKey], let lastName = profileData[Constants.Keys.LastNameKey], let gender = profileData[Constants.Keys.GenderKey] {
                let userModel = UserModel(firstName: firstName, lastName: lastName, gender: gender)
                if let birthday = profileData[Constants.Keys.BirthdayKey] {
                    let dateformatter = DateFormatter()
                    dateformatter.dateFormat = "yyyy-MM-dd"
                    userModel.setBirthday(birthday: dateformatter.date(from: birthday)!)
                }
                if let education = profileData[Constants.Keys.EducationKey] {
                    userModel.setEducation(education: education)
                }
                if let profileImageURL = profileData[Constants.Keys.ProfileImageURLKey] {
                    let storageRef = Constants.refs.storage.reference(forURL: profileImageURL)
                    storageRef.getData(maxSize: 4096*4096, completion: { (data, error) in
                        if let d = data {
                            let profileImage = UIImage(data: d)
                            userModel.setProfileImage(image: profileImage!)
                            encodeAndSaveToCache(userModel)
                            completion()
                        }
                    })
                } else {
                    userModel.setProfileImage(image: UIImage(named: "cat")!)
                    encodeAndSaveToCache(userModel)
                    completion()
                }
                
            }
            else {
                let userModel = UserModel(firstName: "Super", lastName: "Cat", gender: "Who knows")
                userModel.setProfileImage(image: UIImage(named: "cat")!)
                encodeAndSaveToCache(userModel)
                completion()
            }
            
            profileRef.removeAllObservers()
            
        }
    }
}

func fetchAvatar(userNameHash: String, completion: @escaping (UIImage) -> Void) {
    let profileRef = Constants.refs.databaseUsers.child(userNameHash)
    
    profileRef.observe(.value) { snapshot in
        if let profileData = snapshot.value as? [String: String] {
            if let profileImageURL = profileData[Constants.Keys.ProfileImageURLKey] {
                let storageRef = Constants.refs.storage.reference(forURL: profileImageURL)
                storageRef.getData(maxSize: 4096*4096, completion: { (data, error) in
                    if let d = data {
                        let profileImage = UIImage(data: d)
                        completion(profileImage!)
                    }
                })
            } else {
                let cat = UIImage(named: "cat")
                completion(cat!)
            }
            
            profileRef.removeAllObservers()
            
        }
    }
}
