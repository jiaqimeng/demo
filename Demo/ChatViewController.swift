//
//  ChatViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/5/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import ChameleonFramework
import Firebase

class ChatViewController: JSQMessagesViewController {
    
    var chatGroup: ChatGroup! {
        didSet {
            chatRoomRef = Constants.refs.databaseChats.child(chatGroup.groupID)
            getMessageFromFIR()
            getLockFromFIR()
        }
    }
    var locked = false
    var lockMessage = ""
    var sensitivity: Float = 0.0
    var messages = [JSQMessage]()
    var scores = [Float]()
    var chatRoomRef: DatabaseReference!
    var userModel: UserModel?
//    var chatRoomID: String! {
//        didSet {
//            chatRoomRef = Constants.refs.databaseChats.child(chatRoomID)
//            getMessageFromFIR()
//        }
//    }
    var userAvatars = [String: UIImage]()
    
    var googleEmotionAPI = EmotionAnalysis()
    
    fileprivate func getLockFromFIR() {
        let lock = chatRoomRef.child("lock")
        lock.observe(.value) { snapshot in
            if let data = snapshot.value as? [String: Any],
            let locked = data[Constants.Keys.GroupLockKey] as? Bool,
            let lockMessage = data[Constants.Keys.GroupLockMessageKey] as? String,
            let sensitivity = data[Constants.Keys.SentimentSensitivity] as? Float {
                self.locked = locked
                self.lockMessage = lockMessage
                self.sensitivity = sensitivity
            }
        }
    }
    
    fileprivate func getMessageFromFIR() {
        let query = chatRoomRef.queryLimited(toLast: 100)
        
        query.observe(.childAdded, with: { [weak self] snapshot in
            
            if  let data = snapshot.value as? [String: Any],
                let id = data["sender_id"] as? String,
                let name = data["name"] as? String,
                let text = data["text"] as? String,
                let sentimentScore = data["score"] as? String,
                !text.isEmpty {
                if let message = JSQMessage(senderId: id, displayName: name, text: text) {
                    self?.messages.append(message)
                    self?.scores.append(Float(sentimentScore)!)
                    if (self?.userAvatars[id]) != nil {
                        self?.finishSendingMessage()
                    }
                    else {
                        fetchAvatar(userNameHash: id.md5()) { avatar in
                            self?.userAvatars[id] = avatar
                            self?.finishReceivingMessage()
                        }
                    }
                }
                
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        
        if  let id = defaults.string(forKey: "username"),
            let name = defaults.string(forKey: "alias")
        {
            senderId = id
            senderDisplayName = name
        }
        else
        {
            senderId = defaults.string(forKey: "username")
            senderDisplayName = ""
        }
        showDisplayNameDialog()
        title = chatGroup.groupName
        
        let groupInfoButton = UIButton(type: .custom)
        groupInfoButton.setImage(UIImage(named: "groupInfo"), for: .normal)
        groupInfoButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        groupInfoButton.addTarget(self, action: #selector(showGroupInfoViewController), for: .touchUpInside)
        let groupInfoButtonItem = UIBarButtonItem(customView: groupInfoButton)
        self.navigationItem.setRightBarButton(groupInfoButtonItem, animated: true)
        
        inputToolbar.contentView.leftBarButtonItem = nil
    }
    
    @objc func showGroupInfoViewController(sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "ToGroupInfoSegue", sender: self)
    }
    
    @objc func showDisplayNameDialog()
    {
        let defaults = UserDefaults.standard
        
        let alert = UIAlertController(title: "Your Display Name", message: "Before you can chat, please choose a display name. Others will see this name when you send chat messages.", preferredStyle: .alert)
        
        alert.addTextField { textField in
            
            if let name = defaults.string(forKey: "alias")
            {
                textField.text = name
            }
            else
            {
                let names = ["Ford", "Arthur", "Zaphod", "Trillian", "Slartibartfast", "Humma Kavula", "Deep Thought"]
                textField.text = names[Int(arc4random_uniform(UInt32(names.count)))]
            }
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self, weak alert] _ in
            
            if let textField = alert?.textFields?[0], !textField.text!.isEmpty {
                
                self?.senderDisplayName = textField.text
                
                defaults.set(textField.text, forKey: "alias")
                defaults.synchronize()
            }
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func showLockDialog() {
        let alert = UIAlertController(title: "Red Lock ON", message: lockMessage, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToGroupInfoSegue" {
            let groupInfoVC = segue.destination as! GroupInfoViewController
            groupInfoVC.chatGroup = chatGroup
            groupInfoVC.locked = locked
            groupInfoVC.lockMessage = lockMessage
            groupInfoVC.sensitivity = sensitivity
        }
        else if segue.identifier == "ToUserProfileSegue" {
            let otherUserInfoVC = segue.destination as! OtherUserInfoViewController
            otherUserInfoVC.userProfile = userModel
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapAvatarImageView avatarImageView: UIImageView!, at indexPath: IndexPath!) {
        //_Id = String(messages[indexPath.item].senderId.md5())
        let senderId = messages[indexPath.item].senderId
        let profileRef = Constants.refs.databaseUsers.child(String(messages[indexPath.item].senderId.md5()))
        profileRef.observe(.value) { snapshot in
            if let profileData = snapshot.value as? [String: String] {
                let firstName = profileData[Constants.Keys.FirstNameKey]!
                print(firstName)
                let lastName = profileData[Constants.Keys.LastNameKey]!
                let gender = profileData[Constants.Keys.GenderKey]!
                let education = profileData[Constants.Keys.EducationKey]!
                
                let userModelCopy = UserModel(firstName: firstName, lastName: lastName, gender: gender)
                userModelCopy.setProfileImage(image: self.userAvatars[senderId!]!)
                userModelCopy.setEducation(education: education)
                self.userModel = userModelCopy
                self.performSegue(withIdentifier: "ToUserProfileSegue", sender: self)
                
                //let vc = OtherUserInfoViewController()
                //vc.userProfile? = userModel
                
                //self.present(vc, animated: true)
                
            } else {
                print("error")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        
        
        googleEmotionAPI.createRequest(text: text) { (score) in
            let message = ["sender_id": senderId, "name": senderDisplayName, "text": text, "score": "\(score)"]
            if self.locked && score <= -0.66 {
                self.showLockDialog()
                return
            }
            let ref = self.chatRoomRef.childByAutoId()
            ref.setValue(message)
            
            self.finishSendingMessage()
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
//        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
        return messages[indexPath.item].senderId == senderId ? createOutgoingSentimentBubble(score: scores[indexPath.row]) : createIncomingSentimentBubble(score: scores[indexPath.row])
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        // setting avatars
        let messageSenderId = self.messages[indexPath.item].senderId
        if let avatar = self.userAvatars[messageSenderId!] {
            return JSQMessagesAvatarImageFactory.avatarImage(with: avatar, diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if let cell = super.collectionView.cellForItem(at: indexPath) as? JSQMessagesCollectionViewCell {
//            cell.cellTopLabel.textInsets.left = 0
//            return cell
//        }
//        return super.collection
//    }
    
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }()
    
    func createIncomingSentimentBubble(score: Float) -> JSQMessagesBubbleImage {
        let adjustedScore = score + sensitivity
        if adjustedScore <= -0.66 {
            return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: FlatRed())
        }
        if adjustedScore <= -0.33 {
            return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: FlatYellow())
        }
        if adjustedScore > -0.33 && score < 0.33 {
            return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: FlatGray())
        }
        else {
            return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: FlatMint())
        }
    }
    func createOutgoingSentimentBubble(score: Float) -> JSQMessagesBubbleImage {
        let adjustedScore = score + sensitivity
        if adjustedScore <= -0.66 {
            return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: FlatRed())
        }
        if adjustedScore <= -0.33 {
            return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: FlatYellow())
        }
        if adjustedScore > -0.33 && score < 0.33 {
            return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: FlatGray())
        }
        else {
            return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: FlatMint())
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
