//
//  ProfileEditViewController.swift
//  Demo
//
//  Created by Meng Jiaqi on 11/18/17.
//  Copyright © 2017 Richard Meng. All rights reserved.
//

import UIKit
import Eureka
import Firebase

class ProfileEditViewController: FormViewController {
    
    var userProfile: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++ Section("Basic Info")
            <<< NameRow(Constants.Keys.FirstNameKey){ row in
                row.title = "First Name"
                row.value = userProfile?.firstName ?? ""
                row.add(rule: RuleRequired())
            }
            <<< NameRow(Constants.Keys.LastNameKey){ row in
                row.title = "Last Name"
                row.value = userProfile?.lastName ?? ""
                row.add(rule: RuleRequired())
            }
            <<< PickerInputRow<String>(Constants.Keys.GenderKey){
                $0.title = "Gender"
                $0.options = Constants.AvailableGenderLevels
                $0.value = userProfile?.gender ?? $0.options.first
                $0.add(rule: RuleRequired())
        }
        form +++ Section("Additional Info")
            <<< DateRow(Constants.Keys.BirthdayKey){ row in
                row.title = "Birthday"
                row.value = userProfile?.birthday ?? Date()
                let formatter = DateFormatter()
                formatter.locale = .current
                formatter.dateStyle = .long
                row.dateFormatter = formatter
            }
            <<< PickerInputRow<String>(Constants.Keys.EducationKey) {
                $0.title = "Education"
                $0.options = Constants.AvailableEducationLevels
                $0.value = userProfile?.education ?? $0.options.first }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let errors = form.validate()
        if errors.count != 0 {
            return
        }
        let id = UserDefaults.standard.string(forKey: "username")!
        var updatedUserInfo = [String: String]()
        
        let firstNameRow = form.rowBy(tag: Constants.Keys.FirstNameKey) as! NameRow
        updatedUserInfo[Constants.Keys.FirstNameKey] = firstNameRow.value!
        let lastNameRow = form.rowBy(tag: Constants.Keys.LastNameKey) as! NameRow
        updatedUserInfo[Constants.Keys.LastNameKey] = lastNameRow.value!
        let genderRow = form.rowBy(tag: Constants.Keys.GenderKey) as! PickerInputRow<String>
        updatedUserInfo[Constants.Keys.GenderKey] = genderRow.value!
        
        if let birthdayRow = form.rowBy(tag: Constants.Keys.BirthdayKey) as? DateRow {
            if let birthday = birthdayRow.value {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                updatedUserInfo[Constants.Keys.BirthdayKey] = dateFormatter.string(from: birthday)
            }
        }
        
        if let educationRow = form.rowBy(tag: Constants.Keys.EducationKey) as? PickerInputRow<String> {
            if let educationBackground = educationRow.value {
                updatedUserInfo[Constants.Keys.EducationKey] = educationBackground
            }
        }
        
        Constants.refs.databaseUsers.child(id.md5()).updateChildValues(updatedUserInfo)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
